﻿package code
{

	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.display.Shape;
	import flash.text.TextFormat;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.Font;
	import flash.geom.*;
	import gs.*;
	import gs.easing.*;
	import com.tutsplus.active.util.Align;

	public class InfoBox extends MovieClip
	{
		private var _container:MovieClip;
		private var _item:Item;
		public var _itemTextField:TextField;
		public var hiddenTF:TextField;
		private var _defaultFormat:TextFormat = new TextFormat();
		private var _proximaNova:Font = new ProximaNovaAltCondReg();
		private var _textFieldXPosition:uint = 43;
		private var _textFieldYPosition:uint = 0;
		private var _textFieldWidth:uint = 242;
		private var _textFieldHeight:uint = 36;
		private var _itemPosition:Number = -115;
		private var _mask:Shape;
		private var _maskWidth:uint = 284;
		private var _maskHeight:uint = 462;
		private var _paddingTop:uint = 120;
		private var _background:Shape;
		private var _maxSpeed:uint = 100;
		private var _speed:Number;
		private var _items:Array = new Array();
		//private var bg:Sprite
		private var _stage:MovieClip;
		public var scrollModel:Array;

		private var _bounds:Rectangle;
		private var _scrolling:Boolean = false;
		private var _infoSlider:InfoSlider;
		private var contentRange:Number;

		private var owner:Document;
		private var secondBox:Boolean = false;

		public function InfoBox(infoArray:Array, myStage:MovieClip, myOwner:Document, isSecondBox:Boolean = false)
		{
			//////Declare initial properties//////
			owner = myOwner;
			secondBox = isSecondBox;
			this.scrollModel = infoArray;
			this._stage = myStage;
			createRollingScroller();

			//////create the slider bar and set all event listeners//////
			_infoSlider = new InfoSlider();
			this.addChild(_infoSlider);
			_bounds = new Rectangle(290,4,0,_maskHeight - _infoSlider.height + 2);
			_infoSlider.x = 290;
			_infoSlider.y = 4;
			TweenMax.to(_infoSlider,.3,{delay:.5, y:4,ease:Sine.easeInOut});
			_infoSlider.addEventListener(MouseEvent.MOUSE_DOWN, startScroll);
			_stage.addEventListener(MouseEvent.MOUSE_UP, stopScroll);
			this.addEventListener(Event.ENTER_FRAME, updateScroll);
		}

		private function startScroll(e:MouseEvent):void
		{
			//////Start scrolling//////
			_scrolling = true;
			_infoSlider.startDrag(false, _bounds);
		}

		private function stopScroll(e:MouseEvent):void
		{
			//////Stops scrolling//////
			_scrolling = false;
			_infoSlider.stopDrag();
		}

		private function updateScroll(e:Event):void
		{
			if (_scrolling == true)
			{
				//////If scrolling is on, scroll the links within the mask by an amount moved by the mouse//////
				var scrollPercent = (Math.abs((_infoSlider.y + 116) - _mask.y))/_mask.height*1.15;
				var _containerY = _mask.y - (scrollPercent * contentRange);
				TweenMax.to(_container, .6, {y:_containerY, ease:Cubic.easeOut});
			}
		}

		public function scaleTextToFitInTextField( txt : TextField ):void
		{
			//////Set max width and height and grab the textFormat//////
			var maxTextWidth:int = _textFieldWidth - 10;
			var maxTextHeight:int = 48;
			var f:TextFormat = txt.getTextFormat();

			//////decrease font size until the text fits//////
			while (txt.textWidth > maxTextWidth || txt.textHeight > maxTextHeight)
			{
				f.size = int(f.size) - 1;
				txt.setTextFormat(f);
			}
			if(f.size <= 22)
			{
				f.size = 22;
				txt.setTextFormat(f);
			}
		}

		private function createRollingScroller():void
		{
			//////Create a container to hold all of the links//////
			_container = new MovieClip();
			this.addChild(_container);
			_container.removeEventListener(MouseEvent.CLICK, sendValue);
			_items = [];

			for (var i = 0; i < scrollModel.length; i++)
			{
				//////Create a new item for each link//////
				_item = new Item();

				//////Create a textfield for each item//////
				_itemTextField = new TextField();
				_itemTextField.x = _textFieldXPosition;
				_itemTextField.y = _textFieldYPosition;
				_itemTextField.selectable = false;
				_itemTextField.width = _textFieldWidth;
				_itemTextField.height = _textFieldHeight;
				_itemTextField.embedFonts = true;
				
				//////Create a fulltext string incase it is too long to read//////
				hiddenTF = new TextField();
				hiddenTF.x = _textFieldXPosition;
				hiddenTF.y = _textFieldYPosition;
				hiddenTF.visible = false;
				hiddenTF.text = scrollModel[i];
				_item.addChild(hiddenTF);

				//////Create the default text format and set the text in each link//////
				_defaultFormat.color = 0xffac37;
				_defaultFormat.font = _proximaNova.fontName;
				_defaultFormat.size = 22;
				_itemTextField.defaultTextFormat = _defaultFormat;
				_itemTextField.text = scrollModel[i];
				_item.itemTxt = scrollModel[i];
				//scaleTextToFitInTextField(_itemTextField);

				//////position, set propoerties and add each item to the container//////
				_item.addChild(_itemTextField);
				_item.y = _itemPosition + 36 * i;
				_item.buttonMode = true;
				_item.mouseChildren = false;
				_container.addEventListener(MouseEvent.CLICK, sendValue);
				_container.addChild(_item);
				_item.textBG.visible = false;
				_items.push(_item);
				
				//////Finds the length of each text line minus 5 characters//////
				var tfLength:int = _itemTextField.text.length - 5;
				if (_itemTextField.textWidth > 240)
				{
					//////if the line is too long, find the characters between the first 13 characters and the last 5 and replace an ellipsis//////
					var tempString:String = _itemTextField.text.substring(13, tfLength);
					var replaceString:String = "..."
					var newString:String = _itemTextField.text.split(tempString).join(replaceString);
					_itemTextField.text = newString;
				}
			}

			//////Create the infobox mask. Set this to mask the container//////
			_mask = new Shape();
			_mask.graphics.beginFill(0xFF0000);
			_mask.graphics.drawRect(2, _itemPosition, _maskWidth, _maskHeight);
			_mask.graphics.endFill();
			_mask.y = _paddingTop;
			addChild(_mask);
			_container.mask = _mask;
			_container.y = _paddingTop;

			//////Set what is shown within the container mask//////
			contentRange = _container.height - _mask.height;
		}

		public function reloadList(infoArray:Array):void
		{
			//////Set an initial small tween to "show" that one can scroll the info//////
			_infoSlider.y = 25;
			_container.y = _paddingTop + 21;
			_container.removeEventListener(MouseEvent.CLICK, sendValue);
			TweenMax.to(_infoSlider,.5,{delay:.5, y:4,ease:Sine.easeInOut});
			TweenMax.to(_container,.5,{delay:.5, y:_paddingTop,ease:Sine.easeInOut});
			_items = [];

			//////Clear any previous info inside the container//////
			if (_container.numChildren > 0)
			{
				for (var j:int = _container.numChildren - 1; j >= 0; j--)
				{
					_container.removeChildAt(j);
				}
			}
			
			for (var i = 0; i < infoArray.length; i++)
			{
				//////Create a new item for each link//////
				_item = new Item();

				//////Create a textfield for each item//////
				_itemTextField = new TextField();
				_itemTextField.x = _textFieldXPosition;
				_itemTextField.y = _textFieldYPosition;
				_itemTextField.selectable = false;
				_itemTextField.width = _textFieldWidth;
				_itemTextField.height = _textFieldHeight;
				_itemTextField.embedFonts = true;
				
				//////Create a fulltext string incase it is too long to read//////
				hiddenTF = new TextField();
				hiddenTF.x = _textFieldXPosition;
				hiddenTF.y = _textFieldYPosition;
				hiddenTF.visible = false;
				hiddenTF.text = infoArray[i];
				_item.addChild(hiddenTF);

				//////Create the default text format and set the text in each link//////
				_defaultFormat.color = 0xffac37;
				_defaultFormat.font = _proximaNova.fontName;
				_defaultFormat.size = 22;
				_itemTextField.defaultTextFormat = _defaultFormat;
				_itemTextField.text = infoArray[i];
				_item.itemTxt = infoArray[i];
				//scaleTextToFitInTextField(_itemTextField);

				//////position, set propoerties and add each item to the container//////
				_item.addChild(_itemTextField);
				_item.y = _itemPosition + 36 * i;
				_item.buttonMode = true;
				_item.mouseChildren = false;
				_container.addEventListener(MouseEvent.CLICK, sendValue);
				_container.addChild(_item);
				_item.textBG.visible = false;
				_items.push(_item);

				//////Finds the length of each text line minus 5 characters//////
				var tfLength:int = _itemTextField.text.length - 5;
				trace("width = " + _itemTextField.textWidth + " " + _itemTextField.text);
				
				if (_itemTextField.textWidth > 240)
				{
					//////if the line is too long, find the characters between the first 13 characters and the last 5 and replace an ellipsis//////
					var tempString:String = _itemTextField.text.substring(13, tfLength);
					var replaceString:String = "..."
					var newString:String = _itemTextField.text.split(tempString).join(replaceString);
					_itemTextField.text = newString;
				}
			}
			//////Set what is shown within the container mask//////
			contentRange = _container.height - _mask.height;
		}

		private function sendValue(e:MouseEvent):void
		{
			//////Create the current value var//////
			var currentValue:String;
			//trace(e.target.itemText);
			
			for (var i = 0; i < _items.length; i++)
			{
				_items[i].textBG.visible = false;
			}
			e.target.textBG.visible = true;
			
			if (e.target is Item)
			{
				//////Get which link was clicked and set currentValue to that//////
				var tempTextField:TextField;
				tempTextField = e.target.getChildAt(2);
				currentValue = tempTextField.text;
				trace("currentValue = " + currentValue);
			}

			//////If there is a second box, set the current value to what was clicked//////
			if (secondBox == true)
			{
				owner.finalNameSet = true;
			}
			//////Tell document to show the value chosen//////
			owner.setCurrentValue(currentValue);
		}

		/*private function movingOver (event:MouseEvent):void {
		_container.removeEventListener(MouseEvent.MOUSE_OVER, movingOver);
		addEventListener(Event.ENTER_FRAME, enterFrame);
		//if (event.target is Item) 
		//TweenMax.to(Item(event.target).item_btn_over, .2, {alpha:1});
		}
		
		private function movingOut (event:MouseEvent):void {
		removeEventListener(Event.ENTER_FRAME, enterFrame);
		_container.addEventListener(MouseEvent.MOUSE_OVER, movingOver);
		//if (event.target is Item)
		//TweenMax.to(Item(event.target).item_btn_over, .2, {alpha:0});
		}*/
	}
}