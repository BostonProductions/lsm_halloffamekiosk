﻿package code
{
	import flash.display.MovieClip;
	import flash.text.TextFormat;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormatAlign;
	import flash.text.Font;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.display.Sprite;
	import flash.utils.ByteArray;
	import flash.display.Loader;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLLoaderDataFormat;
	import flash.geom.Rectangle;
	import flash.events.Event;

	import gs.*;
	import gs.easing.*;
	import flash.sampler.StackFrame;

	public class UpperSearchBG extends MovieClip
	{
		private var nameTextField:TextField;
		private var nameTextField2:TextField;
		private var sportTextField:TextField;
		private var nicknameTextField:TextField;
		private var yearTextField:TextField;
		private var birthDeathTextField:TextField;
		private var hometownTextField:TextField;
		private var almaMaterTextField:TextField;
		private var hallsofFameTextField:TextField;
		public var lowerTextField:TextField;
		private var nameFormat:TextFormat = new TextFormat();
		private var nameFormat2:TextFormat = new TextFormat();
		private var detailsFormat:TextFormat = new TextFormat();
		private var lowerFormat:TextFormat = new TextFormat();
		private var _proximaNova:Font = new ProximaNovaAltCondReg();
		private var nameTextFieldWidth:Number = 750;
		private var nameTextFieldWHeight:Number = 80;
		private var detailsTextFieldWidth:Number = 450;
		private var detailsTextFieldHeight:Number = 45;
		private var detailsTextFieldX:Number = 540;
		private var lowerTextFieldHeight:Number = 930;
		private var detailsTextFieldBuffer:Number = 10;

		private var lowerSearchNameBar:LowerSearchNameBar;

		private var numButtons:Number = 0;

		private var bioButton:LowerButton;
		private var tiesButton:LowerButton;
		private var notableButton:LowerButton;
		private var quotesButton:LowerButton;
		private var nicknamesButton:LowerButton;
		private var photosButton:ImageButton;
		private var otherPhotosButton:ImageButton;
		private var videoButton:VideoButton;
		private var audioButton:AudioButton;

		public var buttonArray:Array = new Array();

		private var quotesArray:Array;
		public var tempQuotesArray:Array = new Array();
		private var quoteLoc:int = 0;

		private var bioString:String;
		private var bioArray:Array;
		public var tempBioArray:Array = new Array();
		private var bioLoc:int = 0;

		private var noteString:String;
		private var noteArray:Array;
		public var tempNoteArray:Array = new Array();
		private var noteLoc:int = 0;
		
		private var nicknamesString:String;
		private var nicknamesArray:Array;
		public var tempNicknamesArray:Array = new Array();
		private var nicknamesLoc:int = 0;
		
		private var tiesString:String;
		private var tiesArray:Array;
		public var tempTiesArray:Array = new Array();
		private var tiesLoc:int = 0;
		
		private var otherPhotoArray:Array;
		private var otherPhotoImageArray:Array;
		public var tempOtherPhotoArray:Array = new Array();
		private var otherPhotoLoc:int = 0;

		private var photoArray:Array;
		private var photoImageArray:Array;
		public var tempPhotoArray:Array = new Array();
		private var photoLoc:int = 0;

		private var videoArray:Array;
		public var tempVideoArray:Array = new Array();
		private var videoLoc:int = 0;

		private var audioArray:Array;
		public var tempAudioArray:Array = new Array();
		private var audioLoc:int = 0;

		private var imageLoader:Loader = new Loader();

		private var path:String;

		private static var m_instance:UpperSearchBG;

		public function UpperSearchBG()
		{
			path = "";
			photoImageArray = new Array();
			
			//////Set up the lowerSearch bar, nameTextField, nameTextField2 and add them//////
			lowerSearchNameBar = new LowerSearchNameBar();
			this.addChild(lowerSearchNameBar);
			lowerSearchNameBar.y = 1960;
			lowerSearchNameBar.x = -150;

			nameTextField = new TextField();
			nameTextField.x = 80;
			nameTextField.y = 160;
			nameTextField.selectable = false;
			nameTextField.width = nameTextFieldWidth;
			nameTextField.height = nameTextFieldWHeight;
			nameTextField.embedFonts = true;

			nameTextField2 = new TextField();
			nameTextField2.selectable = false;
			nameTextField2.width = 1000;
			nameTextField2.x = lowerSearchNameBar.x + (lowerSearchNameBar.width - nameTextField2.width)/2;
			nameTextField2.height = 48;
			nameTextField2.y = lowerSearchNameBar.y + (lowerSearchNameBar.height - nameTextField2.height)/2 - 5;
			nameTextField2.embedFonts = true;
			
			nameFormat.color = 0xFFFFFF;
			nameFormat.font = _proximaNova.fontName;
			nameFormat.size = 60;

			nameFormat2.color = 0xFFFFFF;
			nameFormat2.font = _proximaNova.fontName;
			nameFormat2.size = 43;
			nameFormat2.align = TextFormatAlign.CENTER;

			nameTextField.defaultTextFormat = nameFormat;
			nameTextField2.defaultTextFormat = nameFormat2;

			this.addChild(nameTextField);
			this.addChild(nameTextField2);

			detailsFormat.color = 0xFFAC37;
			detailsFormat.font = _proximaNova.fontName;
			detailsFormat.size = 40;
			
			//////Set up the sportTextField and add them//////
			sportTextField = new TextField();
			sportTextField.x = detailsTextFieldX;
			sportTextField.y = 270;
			sportTextField.selectable = false;
			sportTextField.width = detailsTextFieldWidth;
			sportTextField.height = detailsTextFieldHeight;
			sportTextField.embedFonts = true;

			sportTextField.defaultTextFormat = detailsFormat;
			this.addChild(sportTextField);
			
			//////Set up the nicknameTextField and add them//////
			nicknameTextField = new TextField();
			nicknameTextField.x = detailsTextFieldX;
			nicknameTextField.y = sportTextField.y + detailsTextFieldHeight + detailsTextFieldBuffer;
			nicknameTextField.selectable = false;
			nicknameTextField.width = detailsTextFieldWidth;
			nicknameTextField.height = detailsTextFieldHeight;
			nicknameTextField.embedFonts = true;

			nicknameTextField.defaultTextFormat = detailsFormat;
			this.addChild(nicknameTextField);
			
			//////Set up the yearTextField and add them//////
			yearTextField = new TextField();
			yearTextField.x = detailsTextFieldX;
			yearTextField.y = nicknameTextField.y + detailsTextFieldHeight + detailsTextFieldBuffer;
			yearTextField.selectable = false;
			yearTextField.width = detailsTextFieldWidth;
			yearTextField.height = detailsTextFieldHeight;
			yearTextField.embedFonts = true;
			
			yearTextField.defaultTextFormat = detailsFormat;
			this.addChild(yearTextField);
			
			//////Set up the birthDeathTextField and add them//////
			birthDeathTextField = new TextField();
			birthDeathTextField.x = detailsTextFieldX;
			birthDeathTextField.y = yearTextField.y + detailsTextFieldHeight + detailsTextFieldBuffer;
			birthDeathTextField.selectable = false;
			birthDeathTextField.width = detailsTextFieldWidth;
			birthDeathTextField.height = detailsTextFieldHeight;
			birthDeathTextField.embedFonts = true;

			birthDeathTextField.defaultTextFormat = detailsFormat;
			this.addChild(birthDeathTextField);
			
			//////Set up the hometownTextField and add them//////
			hometownTextField = new TextField();
			hometownTextField.x = detailsTextFieldX;
			hometownTextField.y = birthDeathTextField.y + detailsTextFieldHeight + detailsTextFieldBuffer;
			hometownTextField.selectable = false;
			hometownTextField.width = detailsTextFieldWidth;
			hometownTextField.height = detailsTextFieldHeight;
			hometownTextField.embedFonts = true;

			hometownTextField.defaultTextFormat = detailsFormat;
			this.addChild(hometownTextField);
			
			//////Set up the almaMaterTextField and add them//////
			almaMaterTextField = new TextField();
			almaMaterTextField.x = detailsTextFieldX;
			almaMaterTextField.y = hometownTextField.y + detailsTextFieldHeight + detailsTextFieldBuffer;
			almaMaterTextField.selectable = false;
			almaMaterTextField.width = detailsTextFieldWidth;
			almaMaterTextField.height = detailsTextFieldHeight;
			almaMaterTextField.embedFonts = true;

			almaMaterTextField.defaultTextFormat = detailsFormat;
			this.addChild(almaMaterTextField);
			
			//////Set up the hallsofFameTextField and add them//////
			hallsofFameTextField = new TextField();
			hallsofFameTextField.x = detailsTextFieldX;
			hallsofFameTextField.y = almaMaterTextField.y + detailsTextFieldHeight + detailsTextFieldBuffer;
			hallsofFameTextField.selectable = false;
			hallsofFameTextField.width = detailsTextFieldWidth;
			hallsofFameTextField.height = detailsTextFieldHeight;
			hallsofFameTextField.embedFonts = true;

			hallsofFameTextField.defaultTextFormat = detailsFormat;
			this.addChild(hallsofFameTextField);
			
			//////Set up the lowerTextField and add them//////
			lowerTextField = new TextField();
			lowerTextField.x = 160;
			lowerTextField.y = 900;
			lowerTextField.selectable = false;
			lowerTextField.width = nameTextFieldWidth;
			lowerTextField.height = lowerTextFieldHeight;
			lowerTextField.embedFonts = true;
			lowerTextField.wordWrap = true;

			lowerFormat.color = 0xFFAC37;
			lowerFormat.font = _proximaNova.fontName;
			lowerFormat.size = 37;

			lowerTextField.defaultTextFormat = lowerFormat;
			this.addChild(lowerTextField);
		}

		private function init(bType:String):void
		{
			//////Activate the different menus depending on which one is clicked//////
			if (bType == "BIOGRAPHY")
			{
				tempBioArray = bioArray;
				bioButton.activateMenu();
			}
			if (bType == "NOTABLE STATISTICS")
			{
				notableButton.activateMenu();
			}
			if (bType == "LOUISIANA TIES")
			{
				tiesButton.activateMenu();
			}
			if (bType == "NICKNAMES")
			{
				nicknamesButton.activateMenu();
			}
			if (bType == "QUOTES")
			{
				quotesButton.activateMenu();
			}
			if (bType == "OTHER PHOTOS")
			{
				otherPhotosButton.activateThumbs();
			}
			if (bType == "PHOTOS OF ARTIFACTS")
			{
				photosButton.activateThumbs();
			}
			if (bType == "VIDEO")
			{
				videoButton.activateThumbs();
			}
			if (bType == "AUDIO")
			{
				audioButton.activateThumbs();
			}
		}

		public function loadAthlete(aAthlete:Athlete):void
		{
			//////set some parameters of the text arrays//////
			quotesArray = aAthlete.quotesArray;
			otherPhotoArray = aAthlete.otherPhotoArray;
			photoArray = aAthlete.photoArray;
			videoArray = aAthlete.videosArray;
			audioArray = aAthlete.audioArray;
			bioString = aAthlete.biographyAchievements;
			noteString = aAthlete.notableStatistics;
			tiesString = aAthlete.louisiannaTies;
			nicknamesString = aAthlete.nicknames;
						
			//////set some parameters of the very top text box//////
			nameTextField2.text = aAthlete.fullName.toUpperCase();
			nameTextField2.defaultTextFormat = nameFormat2;
			nameTextField.text = aAthlete.fullName.toUpperCase();
			nameTextField.defaultTextFormat = nameFormat;
			
			//////set some parameters of the categories text box//////
			var categories:String = "";
			for (var i:int = 0; i < aAthlete.categoriesArray.length; i++)
			{
				categories +=  aAthlete.categoriesArray[i];
				if (i < aAthlete.categoriesArray.length - 1)
				{
					categories +=  ", ";
				}
			}

			sportTextField.text = categories.toUpperCase();
			sportTextField.defaultTextFormat = detailsFormat;
			
			//////set some parameters of the nickname text box//////
			var nicknames:String = "";
			for (var j:int = 0; j < aAthlete.nicknamesArray.length; j++)
			{
				nicknames +=  aAthlete.nicknamesArray[j];
				if (j < aAthlete.nicknamesArray.length - 1)
				{
					nicknames +=  ", ";
				}
			}

			nicknameTextField.text = nicknames.toUpperCase();
			nicknameTextField.defaultTextFormat = detailsFormat;

			yearTextField.text = "Inducted " + aAthlete.yearInducted;
			yearTextField.defaultTextFormat = detailsFormat;

			birthDeathTextField.text = aAthlete.birth + " - " + aAthlete.death;
			birthDeathTextField.defaultTextFormat = detailsFormat;

			hometownTextField.text = "Home Town: " + aAthlete.homeTown.toUpperCase();
			hometownTextField.defaultTextFormat = detailsFormat;
			
			//////set some parameters of the almaMater text box//////
			var almaMaters:String = "";
			for (var k:int = 0; k < aAthlete.almaMaterArray.length; k++)
			{
				almaMaters +=  aAthlete.almaMaterArray[k];
				if (k < aAthlete.almaMaterArray.length - 1)
				{
					almaMaters +=  ", ";
				}
			}

			almaMaterTextField.text = "Alma Mater: " + aAthlete.almaMaterArray[0].toUpperCase();
			almaMaterTextField.defaultTextFormat = detailsFormat;
			
			//////set some parameters of the hallOfFame text box//////
			var hallsoffame:String = "";
			for (var m:int = 0; m < aAthlete.otherHallsofFameArray.length; m++)
			{
				hallsoffame +=  aAthlete.otherHallsofFameArray[m];
				if (m < aAthlete.otherHallsofFameArray.length - 1)
				{
					hallsoffame +=  ", ";
				}
			}
		
			hallsofFameTextField.text = aAthlete.otherHallsofFameArray[0].toUpperCase();
			hallsofFameTextField.defaultTextFormat = detailsFormat;
			
			//////set the text to fit in the very top text box//////
			scaleTextToFitInTextField(nameTextField, nameTextFieldWidth);
			scaleTextToFitInTextField(sportTextField, detailsTextFieldWidth);
			scaleTextToFitInTextField(nicknameTextField, detailsTextFieldWidth);
			scaleTextToFitInTextField(yearTextField, detailsTextFieldWidth);
			scaleTextToFitInTextField(birthDeathTextField, detailsTextFieldWidth);
			scaleTextToFitInTextField(hometownTextField, detailsTextFieldWidth);
			scaleTextToFitInTextField(almaMaterTextField, detailsTextFieldWidth);
			scaleTextToFitInTextField(hallsofFameTextField, detailsTextFieldWidth);
			
			//////Set the button parameters//////
			numButtons = 0;
			var needsButtons:Boolean = true;
			var thisx:Number;
			var thisy:Number = 2150;
			if (numButtons <= 3)
			{
				thisx = 220;
			}
			else
			{
				thisx = -175;
			}
			
			//////If biography content, add the button and add to arrays//////
			if (aAthlete.biographyAchievements != null)
			{
				numButtons++;
				bioButton = new LowerButton("BIOGRAPHY",needsButtons,this);
				bioButton.addEventListener(MouseEvent.CLICK, activateBio);
				bioButton.bg.bgTitle.btnTitle.selectable = false;

				var bioArray:Array = new Array(bioButton,"BIOGRAPHY");

				buttonArray.push(bioArray);
			}
			
			//////If Louisianna Ties content, add the button and add to arrays//////
			if (aAthlete.louisiannaTies != null)
			{
				numButtons++;
				tiesButton = new LowerButton("LOUISIANA TIES",needsButtons,this);
				tiesButton.addEventListener(MouseEvent.CLICK, activateTies);
				tiesButton.bg.bgTitle.btnTitle.selectable = false;

				var tiesArray:Array = new Array(tiesButton,"LOUISIANA TIES");

				buttonArray.push(tiesArray);
			}
			
			//////If statistics content, add the button and add to arrays//////
			if (aAthlete.notableStatistics != null)
			{
				numButtons++;
				notableButton = new LowerButton("NOTABLE STATISTICS",needsButtons,this);
				notableButton.addEventListener(MouseEvent.CLICK, activateNote);
				notableButton.bg.bgTitle.btnTitle.selectable = false;

				var notArray:Array = new Array(notableButton,"NOTABLE STATISTICS");

				buttonArray.push(notArray);
			}
			
			//////If nicknames content, add the button and add to arrays//////
			/*if (aAthlete.nicknamesArray[0] != "")
			{
				numButtons++;
				nicknamesButton = new LowerButton("NICKNAMES",needsButtons,this);
				nicknamesButton.addEventListener(MouseEvent.CLICK, activateNicknames);
				nicknamesButton.bg.bgTitle.btnTitle.selectable = false;

				var nicknamesArray:Array = new Array(nicknamesButton,"NICKNAMES");

				buttonArray.push(nicknamesArray);
			}*/
			
			//////If quotes content, add the button and add to arrays//////
			if (aAthlete.quotesArray[0] != "")
			{
				numButtons++;
				quotesButton = new LowerButton("QUOTES",needsButtons,this);
				quotesButton.addEventListener(MouseEvent.CLICK, activateQuote);
				quotesButton.bg.bgTitle.btnTitle.selectable = false;

				var quotesArray:Array = new Array(quotesButton,"QUOTES");

				buttonArray.push(quotesArray);
			}
			
			//////If other photo content, add the button and add to arrays//////
			if (aAthlete.otherPhotoArray[0] != "")
			{
				numButtons++;
				otherPhotosButton = new ImageButton(aAthlete.otherPhotoArray,aAthlete.otherCaptionArray,aAthlete.otherCopyrightArray,needsButtons,this);
				otherPhotosButton.addEventListener(MouseEvent.CLICK, activateOtherPhotos);
				otherPhotosButton.bg.bgTitle.btnTitle.selectable = false;

				var otherPhotosArray:Array = new Array(otherPhotosButton,"OTHER PHOTOS");

				buttonArray.push(otherPhotosArray);
			}
			
			//////If photo content, add the button and add to arrays//////
			if (aAthlete.photoArray[0] != "")
			{
				numButtons++;
				photosButton = new ImageButton(aAthlete.photoArray,aAthlete.captionArray,aAthlete.copyrightArray,needsButtons,this);
				photosButton.addEventListener(MouseEvent.CLICK, activatePhotos);
				photosButton.bg.bgTitle.btnTitle.selectable = false;

				var photosArray:Array = new Array(photosButton,"PHOTOS OF ARTIFACTS");

				buttonArray.push(photosArray);
			}
			
			//////If video content, add the button and add to arrays//////
			if (aAthlete.videosArray[0] != "")
			{
				numButtons++;
				videoButton = new VideoButton(aAthlete.videosArray, aAthlete.vidCaptionArray, aAthlete.vidCopyrightArray, needsButtons, this);
				videoButton.addEventListener(MouseEvent.CLICK, activateVideo);
				videoButton.bg.bgTitle.btnTitle.selectable = false;

				var videoArray:Array = new Array(videoButton,"VIDEO");

				buttonArray.push(videoArray);
			}
			
			//////If audio content, add the button and add to arrays//////
			if (aAthlete.audioArray[0] != "")
			{
				numButtons++;
				audioButton = new AudioButton(aAthlete.audioArray, aAthlete.audioCaptionArray, aAthlete.audioCopyrightArray, needsButtons, this);
				audioButton.addEventListener(MouseEvent.CLICK, activateAudio);
				audioButton.bg.bgTitle.btnTitle.selectable = false;

				var audioArray:Array = new Array(audioButton,"AUDIO");

				buttonArray.push(audioArray);
			}
			//trace("last name " + aAthlete.lastName + ": " + aAthlete.audioArray[0]);
			
			//////Add buttons and transitions//////
			addButtons();
			startTransitions();
			
			//////fill out the bio section with the proper information//////
			lowerTextField.text = bioString;
			bioArray = checkLength(lowerTextField);
			lowerTextField.text = bioArray[0];
			contentTitle.text = "BIOGRAPHY";
			tempBioArray = bioArray;
			bioButton.removeEventListener(MouseEvent.CLICK, activateBio);
			TweenMax.to(bioButton.bg, 1, {frame:49, ease:Linear.easeNone, onComplete:activateBiography});
			
			//////Load the portrait in the top screen//////
			path = aAthlete.officialPortrait;
			if (path != "")
			{
				var loader:Loader = new Loader;
				loader.contentLoaderInfo.addEventListener(Event.COMPLETE,completeHandler);
				loader.load(new URLRequest(path));
			}
		}
		
		public function activateBiography():void
		{
			//////Once buttons are added start init function//////
			bioButton.activateMenu();
		}

		private function startTransitions():void
		{
			//////Staggers the transitions of the second screen buttons depending on how many are applicable//////
			var tweenClose:TimelineMax = new TimelineMax();
			if (numButtons == 1)
			{
				tweenClose.append(TweenMax.to(buttonArray[0][0].bg, 1, {frame:25, ease:Linear.easeNone}));
			}
			if (numButtons == 2)
			{
				tweenClose.append(TweenMax.to(buttonArray[0][0].bg, 1, {frame:25, ease:Linear.easeNone}));
				tweenClose.append(TweenMax.to(buttonArray[1][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
			}
			if (numButtons == 3)
			{
				tweenClose.append(TweenMax.to(buttonArray[0][0].bg, 1, {frame:25, ease:Linear.easeNone}));
				tweenClose.append(TweenMax.to(buttonArray[1][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[2][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
			}
			if (numButtons == 4)
			{
				tweenClose.append(TweenMax.to(buttonArray[0][0].bg, 1, {frame:25, ease:Linear.easeNone}));
				tweenClose.append(TweenMax.to(buttonArray[1][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[2][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[3][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
			}
			if (numButtons == 5)
			{
				tweenClose.append(TweenMax.to(buttonArray[0][0].bg, 1, {frame:25, ease:Linear.easeNone}));
				tweenClose.append(TweenMax.to(buttonArray[1][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[2][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[3][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[4][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
			}
			if (numButtons == 6)
			{
				tweenClose.append(TweenMax.to(buttonArray[0][0].bg, 1, {frame:25, ease:Linear.easeNone}));
				tweenClose.append(TweenMax.to(buttonArray[1][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[2][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[3][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[4][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[5][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
			}
			if (numButtons == 7)
			{
				tweenClose.append(TweenMax.to(buttonArray[0][0].bg, 1, {frame:25, ease:Linear.easeNone}));
				tweenClose.append(TweenMax.to(buttonArray[1][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[2][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[3][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[4][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[5][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[6][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
			}
			if (numButtons == 8)
			{
				tweenClose.append(TweenMax.to(buttonArray[0][0].bg, 1, {frame:25, ease:Linear.easeNone}));
				tweenClose.append(TweenMax.to(buttonArray[1][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[2][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[3][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[4][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[5][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[6][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[7][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
			}
			if (numButtons == 9)
			{
				tweenClose.append(TweenMax.to(buttonArray[0][0].bg, 1, {frame:25, ease:Linear.easeNone}));
				tweenClose.append(TweenMax.to(buttonArray[1][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[2][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[3][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[4][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[5][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[6][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[7][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
				tweenClose.append(TweenMax.to(buttonArray[8][0].bg, 1, {frame:25, ease:Linear.easeNone}), -.75);
			}
		}

		private function completeHandler(event:Event):void
		{
			//////Create new movieClip to hold the loader data//////
			var imageHolder:MovieClip = new MovieClip();
			imageHolder.addChild(event.target.loader);

			//////Resize the image if larger than the portrait container//////;
			if (imageHolder.width >= portrait.width || imageHolder.height >= portrait.height)
			{
				imageHolder = MediaResizer.ResizeItem(imageHolder,portrait.height - 15,portrait.width - 15);
			}

			//////Center the image inside the portrait container//////
			imageHolder.x = portrait.x - imageHolder.width / 2;
			imageHolder.y = portrait.y - imageHolder.height / 2;
			portrait.visible = false;
			addChild(imageHolder);
		}

		private function checkBioButton():void
		{
			//////reset all the tweens and hide assets//////
			if (bioButton != null)
			{
				TweenMax.to(bioButton.bg, 1, {frame:25, ease:Linear.easeNone});
				contDisplay.visible = false;
				TweenNano.to(bioButton.prev_btn,.3,{alpha:0,ease:Sine.easeInOut});
				TweenNano.to(bioButton.next_btn,.3,{alpha:0,ease:Sine.easeInOut});

				bioButton.addEventListener(MouseEvent.CLICK, activateBio);
			}
		}
		
		private function checkTiesButton():void
		{
			//////reset all the tweens and hide assets//////
			if (tiesButton != null)
			{
				TweenMax.to(tiesButton.bg, 1, {frame:25, ease:Linear.easeNone});
				contDisplay.visible = false;
				TweenNano.to(tiesButton.prev_btn,.3,{alpha:0,ease:Sine.easeInOut});
				TweenNano.to(tiesButton.next_btn,.3,{alpha:0,ease:Sine.easeInOut});

				tiesButton.addEventListener(MouseEvent.CLICK, activateTies);
			}
		}

		private function checkNotesButton():void
		{
			//////reset all the tweens and hide assets//////
			if (notableButton != null)
			{
				TweenMax.to(notableButton.bg, 1, {frame:25, ease:Linear.easeNone});
				contDisplay.visible = false;
				TweenNano.to(notableButton.prev_btn,.3,{alpha:0,ease:Sine.easeInOut});
				TweenNano.to(notableButton.next_btn,.3,{alpha:0,ease:Sine.easeInOut});

				notableButton.addEventListener(MouseEvent.CLICK, activateNote);
			}
		}

		private function checkQuotesButton():void
		{
			//////reset all the tweens and hide assets//////
			if (quotesButton != null)
			{
				TweenMax.to(quotesButton.bg, 1, {frame:25, ease:Linear.easeNone});
				contDisplay.visible = false;
				TweenNano.to(quotesButton.prev_btn,.3,{alpha:0,ease:Sine.easeInOut});
				TweenNano.to(quotesButton.next_btn,.3,{alpha:0,ease:Sine.easeInOut});

				quotesButton.addEventListener(MouseEvent.CLICK, activateQuote);
			}
		}
		
		private function checkNicknamesButton():void
		{
			//////reset all the tweens and hide assets//////
			if (nicknamesButton != null)
			{
				TweenMax.to(nicknamesButton.bg, 1, {frame:25, ease:Linear.easeNone});
				contDisplay.visible = false;
				TweenNano.to(nicknamesButton.prev_btn,.3,{alpha:0,ease:Sine.easeInOut});
				TweenNano.to(nicknamesButton.next_btn,.3,{alpha:0,ease:Sine.easeInOut});

				nicknamesButton.addEventListener(MouseEvent.CLICK, activateNicknames);
			}
		}

		private function checkPhotosButton():void
		{
			//////reset all the tweens and hide assets//////
			if (photosButton != null)
			{
				TweenMax.to(photosButton.bg, 1, {frame:25, ease:Linear.easeNone});
				contDisplay.visible = false;
				TweenNano.to(photosButton.prev_btn,.3,{alpha:0,ease:Sine.easeInOut});
				TweenNano.to(photosButton.next_btn,.3,{alpha:0,ease:Sine.easeInOut});

				photosButton.addEventListener(MouseEvent.CLICK, activatePhotos);
				photosButton.deactivateThumbs();
			}
		}
		
		private function checkOtherPhotosButton():void
		{
			//////reset all the tweens and hide assets//////
			if (otherPhotosButton != null)
			{
				TweenMax.to(otherPhotosButton.bg, 1, {frame:25, ease:Linear.easeNone});
				contDisplay.visible = false;
				TweenNano.to(otherPhotosButton.prev_btn,.3,{alpha:0,ease:Sine.easeInOut});
				TweenNano.to(otherPhotosButton.next_btn,.3,{alpha:0,ease:Sine.easeInOut});

				otherPhotosButton.addEventListener(MouseEvent.CLICK, activateOtherPhotos);
				otherPhotosButton.deactivateThumbs();
			}
		}

		private function checkVideoButton():void
		{
			//////reset all the tweens and hide assets//////
			if (videoButton != null)
			{
				TweenMax.to(videoButton.bg, 1, {frame:25, ease:Linear.easeNone});
				contDisplay.visible = false;
				TweenNano.to(videoButton.prev_btn,.3,{alpha:0,ease:Sine.easeInOut});
				TweenNano.to(videoButton.next_btn,.3,{alpha:0,ease:Sine.easeInOut});

				videoButton.addEventListener(MouseEvent.CLICK, activateVideo);
				videoButton.deactivateThumbs();
			}
		}

		private function checkAudioButton():void
		{
			//////reset all the tweens and hide assets//////
			if (audioButton != null)
			{
				TweenMax.to(audioButton.bg, 1, {frame:25, ease:Linear.easeNone});
				contDisplay.visible = false;
				TweenNano.to(audioButton.prev_btn,.3,{alpha:0,ease:Sine.easeInOut});
				TweenNano.to(audioButton.next_btn,.3,{alpha:0,ease:Sine.easeInOut});

				audioButton.addEventListener(MouseEvent.CLICK, activateAudio);
				audioButton.deactivateThumbs();
			}
		}

		private function activateBio(e:MouseEvent):void
		{
			//////fill out the bio section with the proper information//////
			lowerTextField.text = bioString;
			bioArray = checkLength(lowerTextField);
			lowerTextField.text = bioArray[0];
			bioButton.removeEventListener(MouseEvent.CLICK, activateBio);
			bioLoc = 0;
			contentTitle.text = "BIOGRAPHY";
			TweenMax.to(bioButton.bg, 1, {frame:49, ease:Linear.easeNone, onComplete:init, onCompleteParams:["BIOGRAPHY"]});
			
			//////close out all other buttons//////
			checkNotesButton();
			checkQuotesButton();
			checkOtherPhotosButton();
			checkPhotosButton();
			checkVideoButton();
			checkAudioButton();
			checkTiesButton();
			checkNicknamesButton();
		}
		
		private function activateTies(e:MouseEvent):void
		{
			//////fill out the bio section with the proper information//////
			lowerTextField.text = tiesString;
			tiesArray = checkLength(lowerTextField);
			lowerTextField.text = tiesArray[0];
			tiesButton.removeEventListener(MouseEvent.CLICK, activateTies);
			tiesLoc = 0;
			contentTitle.text = "LOUISIANA TIES";
			tempTiesArray = tiesArray;
			TweenMax.to(tiesButton.bg, 1, {frame:49, ease:Linear.easeNone, onComplete:init, onCompleteParams:["LOUISIANA TIES"]});
			
			//////close out all other buttons//////
			checkBioButton();
			checkNotesButton();
			checkQuotesButton();
			checkOtherPhotosButton();
			checkPhotosButton();
			checkVideoButton();
			checkAudioButton();
			checkNicknamesButton();
		}

		private function activateNote(e:MouseEvent):void
		{
			lowerTextField.text = noteString;
			noteArray = checkLength(lowerTextField);
			lowerTextField.text = noteArray[0];
			noteLoc = 0;
			contentTitle.text = "NOTABLE STATISTICS";
			TweenMax.to(notableButton.bg, 1, {frame:49, ease:Linear.easeNone, onComplete:init, onCompleteParams:["NOTABLE STATISTICS"]});

			tempNoteArray = noteArray;
			notableButton.removeEventListener(MouseEvent.CLICK, activateNote);

			//////close out all other buttons//////
			checkBioButton();
			checkQuotesButton();
			checkPhotosButton();
			checkOtherPhotosButton();
			checkVideoButton();
			checkAudioButton();
			checkTiesButton();
			checkNicknamesButton();
		}

		private function activateQuote(e:MouseEvent):void
		{
			lowerTextField.text = quotesArray[0];
			quoteLoc = 0;
			contentTitle.text = "QUOTES";
			TweenMax.to(quotesButton.bg, 1, {frame:49, ease:Linear.easeNone, onComplete:init, onCompleteParams:["QUOTES"]});

			tempQuotesArray = quotesArray;
			quotesButton.removeEventListener(MouseEvent.CLICK, activateQuote);
			
			//////close out all other buttons//////
			checkBioButton();
			checkNotesButton();
			checkPhotosButton();
			checkOtherPhotosButton();
			checkVideoButton();
			checkAudioButton();
			checkTiesButton();
			checkNicknamesButton();
		}
		
		private function activateNicknames(e:MouseEvent):void
		{
			lowerTextField.text = nicknamesArray[0];
			nicknamesLoc = 0;
			contentTitle.text = "NICKNAMES";
			TweenMax.to(nicknamesButton.bg, 1, {frame:49, ease:Linear.easeNone, onComplete:init, onCompleteParams:["NICKNAMES"]});

			tempNicknamesArray = nicknamesArray;
			nicknamesButton.removeEventListener(MouseEvent.CLICK, activateNicknames);
			
			//////close out all other buttons//////
			checkBioButton();
			checkNotesButton();
			checkPhotosButton();
			checkQuotesButton();
			checkOtherPhotosButton();
			checkVideoButton();
			checkAudioButton();
			checkTiesButton();
		}

		private function activatePhotos(e:MouseEvent):void
		{
			lowerTextField.text = "";
			photoLoc = 0;
			contentTitle.text = "PHOTOS OF ARTIFACTS";
			TweenMax.to(photosButton.bg, 1, {frame:49, ease:Linear.easeNone, onComplete:init, onCompleteParams:["PHOTOS OF ARTIFACTS"]});
			photosButton.removeEventListener(MouseEvent.CLICK, activatePhotos);

			//////close out all other buttons//////
			checkBioButton();
			checkNotesButton();
			checkQuotesButton();
			checkOtherPhotosButton();
			checkVideoButton();
			checkAudioButton();
			checkTiesButton();
			checkNicknamesButton();
		}
		
		private function activateOtherPhotos(e:MouseEvent):void
		{
			lowerTextField.text = "";
			otherPhotoLoc = 0;
			contentTitle.text = "OTHER PHOTOS";
			TweenMax.to(otherPhotosButton.bg, 1, {frame:49, ease:Linear.easeNone, onComplete:init, onCompleteParams:["OTHER PHOTOS"]});
			otherPhotosButton.removeEventListener(MouseEvent.CLICK, activateOtherPhotos);

			//////close out all other buttons//////
			checkBioButton();
			checkNotesButton();
			checkQuotesButton();
			checkPhotosButton();
			checkVideoButton();
			checkAudioButton();
			checkTiesButton();
			checkNicknamesButton();
		}

		private function activateVideo(e:MouseEvent):void
		{
			tempVideoArray = videoArray;
			lowerTextField.text = "";
			videoLoc = 0;
			contentTitle.text = "VIDEO";
			videoButton.removeEventListener(MouseEvent.CLICK, activateVideo);
			TweenMax.to(videoButton.bg, 1, {frame:49, ease:Linear.easeNone, onComplete:init, onCompleteParams:["VIDEO"]});

			//////close out all other buttons//////
			checkBioButton();
			checkNotesButton();
			checkQuotesButton();
			checkOtherPhotosButton();
			checkPhotosButton();
			checkAudioButton();
			checkTiesButton();
			checkNicknamesButton();
		}

		private function activateAudio(e:MouseEvent):void
		{
			tempAudioArray = audioArray;
			lowerTextField.text = "";
			audioLoc = 0;
			contentTitle.text = "AUDIO";
			audioButton.removeEventListener(MouseEvent.CLICK, activateAudio);
			TweenMax.to(audioButton.bg, 1, {frame:49, ease:Linear.easeNone, onComplete:init, onCompleteParams:["AUDIO"]});

			//////close out all other buttons//////
			checkBioButton();
			checkNotesButton();
			checkQuotesButton();
			checkOtherPhotosButton();
			checkPhotosButton();
			checkVideoButton();
			checkTiesButton();
			checkNicknamesButton();
		}

		private function addButtons():void
		{
			var thisx:Number;
			var thisy:Number = 2050;
			if (numButtons <= 3)
			{
				thisx = 220;
			}
			else
			{
				thisx = -75;
			}
			
			//////sets parameters for each visible button//////
			for (var i:int = 0; i < buttonArray.length; i++)
			{
				this.addChild(buttonArray[i][0]);
				switch (numButtons)
				{
					case 1:
						buttonArray[i][0].x = thisx;
						buttonArray[i][0].y = thisy + 25 + ((buttonArray[i][0].height + 50));
						buttonArray[i][0].bg.bgTitle.btnTitle.text = buttonArray[i][1];
						break;

					case 2:
						buttonArray[i][0].x = thisx;
						buttonArray[i][0].y = thisy + 25 + ((buttonArray[i][0].height + 50) *i);
						buttonArray[i][0].bg.bgTitle.btnTitle.text = buttonArray[i][1];
						break;

					case 3 :
						buttonArray[i][0].x = thisx;
						buttonArray[i][0].y = thisy + 25 + ((buttonArray[i][0].height + 50) *i);
						buttonArray[i][0].bg.bgTitle.btnTitle.text = buttonArray[i][1];
						break;

					case 4:
						if (i < 2)
						{
							buttonArray[i][0].x = thisx;
							buttonArray[i][0].y = thisy + 25 + ((buttonArray[i][0].height + 50) *i);
							buttonArray[i][0].bg.bgTitle.btnTitle.text = buttonArray[i][1];
						}
						if (i >= 2)
						{
							buttonArray[i][0].x = thisx + buttonArray[i][0].width + 255;
							buttonArray[i][0].y = buttonArray[i - 2][0].y;
							buttonArray[i][0].bg.bgTitle.btnTitle.text = buttonArray[i][1];
						}
						break;

					case 5:
						if (i < 3)
						{
							buttonArray[i][0].x = thisx;
							buttonArray[i][0].y = thisy + 25 + ((buttonArray[i][0].height + 50) *i);
							buttonArray[i][0].bg.bgTitle.btnTitle.text = buttonArray[i][1];
						}
						if (i >= 3)
						{
							buttonArray[i][0].x = thisx + buttonArray[i][0].width + 255;
							buttonArray[i][0].y = buttonArray[i-3][0].y;
							buttonArray[i][0].bg.bgTitle.btnTitle.text = buttonArray[i][1];
						}
						break;

					case 6:
						if (i < 3)
						{
							buttonArray[i][0].x = thisx;
							buttonArray[i][0].y = thisy + 25 + ((buttonArray[i][0].height + 50) *i);
							buttonArray[i][0].bg.bgTitle.btnTitle.text = buttonArray[i][1];
						}
						if (i >= 3)
						{
							buttonArray[i][0].x = thisx + buttonArray[i][0].width + 255;
							buttonArray[i][0].y = buttonArray[i - 3][0].y;
							buttonArray[i][0].bg.bgTitle.btnTitle.text = buttonArray[i][1];
						}
						break;
						
					case 7:
						if (i < 4)
						{
							buttonArray[i][0].x = thisx;
							buttonArray[i][0].y = thisy + ((buttonArray[i][0].height + 15) *i);
							buttonArray[i][0].bg.bgTitle.btnTitle.text = buttonArray[i][1];
						}
						if (i >= 4)
						{
							buttonArray[i][0].x = thisx + buttonArray[i][0].width + 255;
							buttonArray[i][0].y = buttonArray[i - 4][0].y-10;
							buttonArray[i][0].bg.bgTitle.btnTitle.text = buttonArray[i][1];
						}
						break;
						
					case 8:
						if (i < 4)
						{
							buttonArray[i][0].x = thisx;
							buttonArray[i][0].y = thisy + ((buttonArray[i][0].height + 15) *i);
							buttonArray[i][0].bg.bgTitle.btnTitle.text = buttonArray[i][1];
						}
						if (i >= 4)
						{
							buttonArray[i][0].x = thisx + buttonArray[i][0].width + 255;
							buttonArray[i][0].y = buttonArray[i - 4][0].y-10;
							buttonArray[i][0].bg.bgTitle.btnTitle.text = buttonArray[i][1];
						}
						break;
					
					case 9:
						if (i < 4)
						{
							buttonArray[i][0].x = thisx;
							buttonArray[i][0].y = thisy + ((buttonArray[i][0].height + 15) *i);
							buttonArray[i][0].bg.bgTitle.btnTitle.text = buttonArray[i][1];
						}
						if (i >= 4)
						{
							buttonArray[i][0].x = thisx + buttonArray[i][0].width + 255;
							buttonArray[i][0].y = buttonArray[i - 4][0].y-10;
							buttonArray[i][0].bg.bgTitle.btnTitle.text = buttonArray[i][1];
						}
						break;
					default :
						trace("None of the above were met");
				}
			}
		}

		private function checkLength(myTextField:TextField):Array
		{
			var myArray = new Array();
			var tempTextField:TextField = myTextField;
			if (tempTextField.numLines < 20)
			{
				var text2 = new String(tempTextField.text);
				myArray.push(text2);
			}
			else
			{
				var forLoop:int = Math.ceil(tempTextField.numLines / 20);
				var text1:String;
				var textLength:int = 0;
				var numberLines:int;

				for (var k:int = 0; k < forLoop; k++)
				{
					textLength = 0;
					if (tempTextField.numLines > 20)
					{
						numberLines = 20;
					}
					else
					{
						numberLines = tempTextField.numLines;
					}
					for (var i:int = 0; i < numberLines; i++)
					{
						textLength +=  tempTextField.getLineLength(i);
					}

					text1 = tempTextField.text.slice(0,textLength);
					myArray.push(text1);
					tempTextField.text = tempTextField.text.slice(textLength,tempTextField.length);
				}
			}
			return myArray;
		}

		private function scaleTextToFitInTextField( txt : TextField, txtWidth:Number ):void
		{
			var maxTextWidth:int = txtWidth - 5;
			var maxTextHeight:int = 80;

			var f:TextFormat = txt.getTextFormat();

			//decrease font size until the text fits
			while (txt.textWidth > maxTextWidth || txt.textHeight > maxTextHeight)
			{
				f.size = int(f.size) - 1;
				txt.setTextFormat(f);
			}
		}
	}

}