﻿package code
{	
	import flash.events.Event;
	import flash.net.URLRequest;
	import flash.net.URLLoader;
	import flash.events.EventDispatcher;
	
	class ScrollModel extends EventDispatcher {
		
		public var loader:URLLoader;
		public var data:XML;
		public var items:XMLList;
		
		public static const MODEL_UPDATE:String = "modelChange";
		
		public function ScrollModel() {
			loader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, dataLoaded);
		}
		
		public function load(req:URLRequest):void {
			loader.load(req);
		}
		
		private function dataLoaded(event:Event):void {			
			data = new XML(event.target.data);			
			items = data.item;
			dispatchEvent(new Event(ScrollModel.MODEL_UPDATE));
		}
	}	
}