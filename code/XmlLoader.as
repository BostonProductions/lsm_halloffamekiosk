﻿package code
{
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.display.Sprite;
	
	public class XmlLoader extends Sprite
	{
		public static const XML_LOADED:String = "xmlLoaded";
		public static const XML_ERROR:String = "xmlError";
		
		public var xml:XML;
		private var xmlLoader:URLLoader = new URLLoader();
		private var xmlRequest:URLRequest;
		
		public function loadXml(xmlPath:String)
		{
			xmlRequest = new URLRequest(xmlPath);
			xmlLoader.addEventListener(IOErrorEvent.IO_ERROR, errorLoadingXml);
			xmlLoader.addEventListener(Event.COMPLETE, xmlLoaded);
			xmlLoader.load(xmlRequest);
		}
		private function xmlLoaded(e:Event):void
		{
			xmlLoader.removeEventListener(IOErrorEvent.IO_ERROR, errorLoadingXml);
			xmlLoader.removeEventListener(Event.COMPLETE, xmlLoaded);
			xml = new XML(e.target.data);
			dispatchEvent(new Event(XML_LOADED));
		}
		private function errorLoadingXml(e:IOErrorEvent):void
		{
			xmlLoader.removeEventListener(IOErrorEvent.IO_ERROR, errorLoadingXml);
			xmlLoader.removeEventListener(Event.COMPLETE, xmlLoaded);
			dispatchEvent(new Event(XML_ERROR));
		}
	}
}