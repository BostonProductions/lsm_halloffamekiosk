﻿package code
{
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Bitmap;
	
	public class MediaResizer
	{
		// take in loader, height, and width params
		public static function ResizeItem(item:DisplayObject, maxHeight:Number, maxWidth:Number):*
		{
			// record the original height and width which is needed to determine a ratio
			var origHeight:Number = item.height;
			var origWidth:Number = item.width;
			
			// if the maxWidth is greater than the maxHeight, set the height to the max height and the width to the percentage of width to height
			
			if(origHeight > origWidth)
			{
				item.height = maxHeight;
				item.width = maxHeight * (origWidth / origHeight);
				if(item.width > maxWidth)
				{
					var tempWidth:Number = item.width;
					item.width = maxWidth;
					item.height = maxWidth * (item.height/tempWidth);
				}
			}
			else if(origHeight < origWidth)
			{
				item.width = maxWidth;
				item.height = maxWidth * (origHeight / origWidth);
				if(item.height > maxHeight)
				{
					var tempHeight:Number = item.height;
					item.height = maxHeight;
					item.width = maxHeight * (item.width/tempHeight);
				}
			}
			
			else
			{
				item.height = maxHeight;
				item.width = maxWidth;
			}
			return item;
		}
		
	}
}